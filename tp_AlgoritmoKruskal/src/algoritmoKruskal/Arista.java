package algoritmoKruskal;

public class Arista implements Comparable<Arista> {

    private Vertice vertice1, vertice2;
    private int peso;
    
    /**
     * Se detalla la construcci�n de la arista. El vertice lexicograficamente
     * menor se representa en la parte superior del grafo (vertice1)
     * y el vertice cuya etiqueta es lexicograficamente mayor en la inferior (vertice2)
     *
     * @param vertice1. Extremo o v�rtice de la arista
     * @param vertice2. Segundo v�rtice para formar la arista
     * @param peso. Define el coste de ir desde el vertice1 al vertice2 y viceversa(arista no dirigida)
     **/
    public Arista(Vertice vertice1, Vertice vertice2, int peso)
    {
	if(vertice1.getEtiqueta().compareTo(vertice2.getEtiqueta()) <= 0)
	    {
		this.vertice1 = vertice1;
		this.vertice2 = vertice2;
	    }
	else
	    {
		this.vertice1 = vertice2;
		this.vertice2 = vertice1;
	    }

     	this.peso = peso;
		
    }
    
    /**
     * Comparamos el coste de esta arista con el coste
     * de otra arista como par�mentro de entrada llamada arista2
     * 
     * @param arista2. Arista con la que comparamos nuestra arista actual
     * @return int. Se devuelve 0 en caso de que ambas tengan el mismo peso
     **/
    public int compareTo(Arista arista2)
    {
	return this.peso - arista2.peso;
    }
    
    /**
     * @return el contenido del atributo vertice1 de tipo Vertice
     **/
    public Vertice getVertice1()
    {
	return this.vertice1;
    }
    
    /**
     * @return el contenido del atributo vertice2 de tipo Vertice
     **/
    public Vertice getVertice2()
    {
	return this.vertice2;
    }

    /**
     * @return el valor de tipo entero del atributo peso
     **/
    public int getPeso()
    {
	return this.peso;
    }
    
    /**
     * @return int. C�digo hash para esta arista
     **/
    public int hashCode()
    {
	return (vertice1.getEtiqueta() + vertice2.getEtiqueta()).hashCode();
    }
}
