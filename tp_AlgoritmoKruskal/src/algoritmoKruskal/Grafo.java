package algoritmoKruskal;

import java.util.HashMap;

/*
Los v�rtices se distinguen por su etiqueta.
* Un par de v�rtices solo permite una arista entre ellos
*/

public class Grafo {

	private HashMap<String, Vertice> vertices;
	private HashMap<Integer, Arista> aristas;
	
	/**
     * Construcci�n de un grafo vac�o
     **/
	public Grafo() 
	{
		this.vertices = new HashMap<String, Vertice>();
		this.aristas = new HashMap<Integer, Arista>();
	}
	
	 /**
     * Inserta una arista entre los vertices v1 y v2
     * y un coste o peso especificado por par�metro de entrada. 
     * La arista se insertar� con �xito siempre que sea �nica y adem�s
     * no haya otra uniendo actualmente v1 y v2. Finalmente v1 y v2 no
     * deben ser el mismo v�rtice (v1 != v2)
     *
     * @param v1. Un extremo de la arista
     * @param v2. Otro extremo de la arista
     * @param peso. Coste para llegar de v1 a v2 o viceversa
     * @return true. Si y solo si la arista no existe previamente
     **/
    public boolean insertarArista(Vertice v1, Vertice v2, int peso)
    {
	if(v1.equals(v2)) //vertices identicos?
	    return false;

	Arista arista = new Arista(v1, v2, peso);

	if(aristas.containsKey(arista.hashCode())) //arista ya est� en el grafo?
	    return false;
	else if( v1.contieneUnVecino(arista) || v2.contieneUnVecino(arista)) //arista ya une a v1 o v2?
	    return false;

	aristas.put(arista.hashCode(), arista);
	v1.insertarVecino(arista);
	v2.insertarVecino(arista);
	return true;
    }

	public HashMap<String, Vertice> getVertices() {
		return vertices;
	}
}
