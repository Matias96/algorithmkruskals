package algoritmoKruskal;

public class Main {

	public static void main(String[] args) {
		
		Grafo g = new Grafo();
		
		Vertice [] v = new Vertice[6];
		char [] etiquetas = {'A', 'B', 'C', 'D', 'E', 'F'};
		
		for(int i = 0; i < v.length; i++)
		{
	    	v[i] = new Vertice(Character.toString(etiquetas[i]));
	     	System.out.println(v[i].toString());
		}
		
		g.insertarArista(v[0], v[1], 3); // A -> B
		g.insertarArista(v[0], v[2], 4); // A -> C
		g.insertarArista(v[1], v[2], 1); // B -> C
	}

}
