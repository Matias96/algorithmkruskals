package algoritmoKruskal;

import java.util.ArrayList;

public class Vertice {

	private ArrayList<Arista> vecindad;
    private String etiqueta;

    public Vertice (String etiqueta)
    {
	this.etiqueta = etiqueta;
	this.vecindad = new ArrayList<Arista>();
    }

    /**
     * A�ade un objeto Arista al array de lista vecindad
     * si y solo este no est� contenido en dicho array de lista 
     * @param arista. Objeto a a�adir
     */
    public void insertarVecino(Arista arista)
    {
	if( !this.vecindad.contains(arista))
	    this.vecindad.add(arista);
    }
    
    /**
     * Comprueba si la arista incide en este vertice
     * @param arista. Objeto a evaluar
     * @return true. Si y solo si el objeto esta contenido en el array de lista vecindad
     */
    public boolean contieneUnVecino(Arista arista)
    {
	return this.vecindad.contains(arista);
    }
    
    /**
     * @return String. Devuelve el valor de la cadena etiqueta
     */
    public String getEtiqueta()
    {
	return this.etiqueta;
    }
    
    public String toString()
    {
	return "Vertice: " + this.etiqueta;
    }
}
